# AWS EC2 instance Terraform module

Ubuntu 18.04 (Bionic Beaver) running latest Docker.

## Features

* Can be run from Linux or Windows (GitBash)
* SSH key generated for each new Instance (not stored in Terraform state)
* Packages upgraded to the latest version
* Running latest Docker CE
* Default user **ubuntu** with a generated password (not stored in Terraform state)
* Sudo with a password
* SSHD running on the non-standard port (optionally)
* Timezone set (optionally)
* Security Group for SSH access

## Requirements

* Subnet (either Private or Public)
* `ssh-keygen`
* [GitBash](https://gitforwindows.org/) (on Windows)

## Usage

    module "new_instance" {
        source        = "git::https://bitbucket.org/terraforming/terraform-aws-docker-on-beaver.git?ref=master"
        deployment_id = "SOMETHING_UNIQUE"
        hostname      = "new-instance"
    }

## Outputs

| Name | Description |
|------|-------------|
| id | Instance ID |
| hostname | Instance hostname |
| public_ip | Instance public IP address (ipv4) |
| ssh_key | Generated SSH key |
| password | Generated file with a password |

## Variables

| Name | Default | Description |
|------|---------|-------------|
| deployment_id | | Module deployment ID (may be generated with [terraform-deployment-id](https://bitbucket.org/terraforming/terraform-deployment-id) module) |
| hostname | | Instance name (can only contain alphanumeric characters, dashes, and periods) |
| ami | "" | Beaver Image (AMI) to use (can be empty) |
| instance_type | t2.micro | Type of Instance to start |
| subnet | | Subnet where Instance will launch |
| instance_profile | "" | IAM instance profile to launch the Instance with (can be empty) |
| sshd_port | 22 | Just say NO to port 22! |
| timezone | Etc/UTC | Instance Timezone to set. Get available timezones with: timedatectl list-timezones |
| reboot | true | Reboot Instance when done (set to false if you need to run your own scripts with remote-exec) |

## Tips

### AMI

Module use the **latest** Beaver AMI. Set this parameter to existing instance's AMI to prevent replacement during updates.

### Subnet

Subnet specify the AZ (thus region) where the instance will be started. 

Public subnet may by created with ```terraform-aws-vpc``` module.

The subnet may be also obtained from the **Default** VPC (created with the AWS account) using
Terraform [aws_vpc](https://www.terraform.io/docs/providers/aws/d/vpc.html) and [aws_subnet_ids](https://www.terraform.io/docs/providers/aws/d/subnet_ids.html) datasources.

E.g.:

    data "aws_vpc" "default" {
        default = true
    }

    data "aws_subnet_ids" "public" {
        vpc_id = "${data.aws_vpc.default.id}"
    }

    output "public_subnet_id" {
        value = "${data.aws_subnet_ids.public.ids[0]}"
    }

### Credentials

Following credentials were generated before Instance startup. It is a good idea to backup them :)

* SSH Key

    Generated SSH key is stored in `.ssh` subdirectory of the Terraform root module.  
    Filename is `.ssh/id_rsa-aws-${deployment_id}`.

* Password

    Generated password for **ubuntu** user is stored in `.ssh` subdirectory of the Terraform root module.  
    Filename is `.ssh/password-aws-${deployment_id}`.

Note: Instance is tagged with Module deployment ID.

### Docker

User **ubuntu** is in the _docker_ group and can run `docker` command without sudo.

### Sudo

User **ubuntu** can use a `sudo` command with a generated password. Password is in `.ssh/password-aws-${deployment_id}`.

### Additional setup

If you would need to run your own scripts in the created Instance, you may use `null_resource`.

1. Set the `reboot` to **false**

        module "new_instance" {
            source   = "git::https://bitbucket.org/terraforming/terraform-aws-docker-on-beaver.git?ref=master"
            hostname = "new-instance"
            reboot   = "false"
        }

2. Run your script with `remote-exec` and (optionally) reboot Instance yourself

        resource "null_resource" "setup-instance" {
            provisioner "remote-exec" {
                inline = [
                    # Run your own script
                    "touch /tmp/have-been-here",
                    "shutdown --reboot +1",
                ]
                connection {
                    host        = "${module.new_instance.public_ip}"
                    # Port is still 22 until reboot or SSHD is restarted
                    port        = "22"
                    # Root login is still working until reboot or SSHD is restarted
                    user        = "root"
                    private_key = "${file("${path.module}/${module.new_instance.ssh_key}")}"
                }
            }
            depends_on = ["module.new_instance"]
        }

### SSH command

Add SSH command to outputs (to use from scripts)

    output "ssh_cmd" {
        value = "ssh -i ${module.new_instance.ssh_key} -p ${var.sshd_port} ubuntu@${module.new_instance.public_ip}"
    }
