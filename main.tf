data "aws_ami" "beaver" {
  most_recent = true

  filter {
    name = "name"

    values = [
      "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*",
    ]
  }

  owners = [
    # Canonical
    "099720109477",
  ]

  filter {
    name = "architecture"

    values = [
      "x86_64",
    ]
  }

  filter {
    name = "root-device-type"

    values = [
      "ebs",
    ]
  }

  filter {
    name = "virtualization-type"

    values = [
      "hvm",
    ]
  }
}

locals {
  ssh_key  = ".ssh/id_rsa-aws-${var.deployment_id}"
  password = ".ssh/password-aws-${var.deployment_id}"

  # For better compatibility the path elements on Win must use only simple slashes
  path_root   = "${replace(path.root, "\\", "/")}"
  path_module = "${replace(path.module, "\\", "/")}"
}

# Not using tls_private_key resource to keep the key only on local machine
# https://www.terraform.io/docs/providers/tls/r/private_key.html
resource "null_resource" "ssh_keygen" {
  triggers = {
    deployment_id = "${var.deployment_id}"
  }

  provisioner "local-exec" {
    command     = "mkdir --parents $(${local.path_module}/cygpath ${local.path_root}/.ssh)"
    interpreter = ["bash", "-c"]
  }

  provisioner "local-exec" {
    command     = "echo n | ssh-keygen -t rsa -b 4096 -C '${var.deployment_id}' -f $(${local.path_module}/cygpath ${local.path_root}/${local.ssh_key}) -N '' || true"
    interpreter = ["bash", "-c"]
  }
}

resource "aws_key_pair" "default" {
  key_name_prefix = "${var.hostname}-${var.deployment_id}-"
  public_key      = "${file("${local.path_root}/${local.ssh_key}.pub")}"
  depends_on      = ["null_resource.ssh_keygen"]
}

resource "random_string" "password" {
  keepers = {
    deployment_id = "${var.deployment_id}"
  }

  length           = 14
  min_upper        = 5
  min_lower        = 5
  min_numeric      = 1
  min_special      = 1
  override_special = "!@#$-+"
}

resource "null_resource" "password_gen" {
  triggers = {
    deployment_id = "${var.deployment_id}"
    password      = "${random_string.password.result}"
  }

  provisioner "local-exec" {
    command     = "mkdir --parents $(${local.path_module}/cygpath ${local.path_root}/.ssh)"
    interpreter = ["bash", "-c"]
  }

  provisioner "local-exec" {
    command     = "echo '${random_string.password.result}' > $(${local.path_module}/cygpath ${local.path_root}/${local.password})"
    interpreter = ["bash", "-c"]
  }
}

data "aws_subnet" "subnet" {
  id = "${var.subnet}"
}

resource "aws_security_group" "sg" {
  vpc_id = "${data.aws_subnet.subnet.vpc_id}"

  tags {
    Name          = "${var.hostname}"
    deployment_id = "${var.deployment_id}"
  }
}

resource "aws_security_group_rule" "ssh_standard_rule" {
  type              = "ingress"
  description       = "SSH access on standard port"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.sg.id}"
}

resource "aws_security_group_rule" "ssh_non_standard_rule" {
  count = "${var.sshd_port == 22 ? 0 : 1}"

  type              = "ingress"
  description       = "SSH access on non-standard port"
  from_port         = "${var.sshd_port}"
  to_port           = "${var.sshd_port}"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.sg.id}"
}

resource "aws_security_group_rule" "outbound_rule" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.sg.id}"
}

module "provisioner_files" {
  source = "git::https://bitbucket.org/terraforming/terraform-docker-on-beaver.git?ref=master"
}

resource "aws_instance" "beaver" {
  ami                  = "${var.ami == "" ? data.aws_ami.beaver.id : var.ami}"
  instance_type        = "${var.instance_type}"
  subnet_id            = "${var.subnet}"
  iam_instance_profile = "${var.instance_profile}"

  vpc_security_group_ids      = ["${aws_security_group.sg.id}"]
  associate_public_ip_address = true

  key_name = "${aws_key_pair.default.key_name}"

  tags {
    Name          = "${var.hostname}"
    deployment_id = "${var.deployment_id}"
    fw            = "allow-ssh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir --parents /var/tmp/terraform-aws-docker-on-beaver",
      "sudo chown ubuntu:ubuntu /var/tmp/terraform-aws-docker-on-beaver",
    ]

    connection {
      user        = "ubuntu"
      private_key = "${file("${path.root}/${local.ssh_key}")}"
    }
  }
}

resource "null_resource" "provisioner_upload" {
  count = "${length(module.provisioner_files.scripts)}"

  triggers {
    deployment_id = "${var.deployment_id}"
    index         = "${count.index}"
  }

  provisioner "file" {
    content     = "${module.provisioner_files.scripts[count.index]}"
    destination = "/var/tmp/terraform-aws-docker-on-beaver/${count.index}-provisioner-script.sh"

    connection {
      host        = "${aws_instance.beaver.public_ip}"
      user        = "ubuntu"
      private_key = "${file("${path.root}/${local.ssh_key}")}"
    }
  }

  depends_on = ["aws_instance.beaver", "module.provisioner_files"]
}

resource "null_resource" "provisioner_launch" {
  triggers = {
    deployment_id = "${var.deployment_id}"
  }

  provisioner "file" {
    content     = "${module.provisioner_files.launcher}"
    destination = "/var/tmp/terraform-aws-docker-on-beaver/launcher.sh"

    connection {
      host        = "${aws_instance.beaver.public_ip}"
      user        = "ubuntu"
      private_key = "${file("${path.root}/${local.ssh_key}")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "cd /var/tmp/terraform-aws-docker-on-beaver",
      "chmod +x *.sh",
      "sudo ./launcher.sh '${var.timezone}' '${random_string.password.result}' '${var.sshd_port}' '${var.reboot}'",
    ]

    connection {
      host        = "${aws_instance.beaver.public_ip}"
      user        = "ubuntu"
      private_key = "${file("${path.root}/${local.ssh_key}")}"
    }
  }

  depends_on = ["null_resource.provisioner_upload"]
}
