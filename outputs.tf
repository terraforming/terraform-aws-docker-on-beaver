output "id" {
  description = "Instance ID"
  value       = "${aws_instance.beaver.id}"
}

output "hostname" {
  description = "Instance hostname"
  value       = "${aws_instance.beaver.public_dns}"
}

output "public_ip" {
  description = "Instance public IP address (ipv4)"
  value       = "${aws_instance.beaver.public_ip}"
}

output "ssh_key" {
  description = "Generated SSH key"
  value       = "${local.ssh_key}"
}

output "password" {
  description = "Generated file with a password"
  value       = "${local.password}"
}
