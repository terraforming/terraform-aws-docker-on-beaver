variable "deployment_id" {
  description = "Module deployment ID (may be generated with terraform-deployment-id module)"
}

variable "hostname" {
  description = "Instance name (can only contain alphanumeric characters, dashes, and periods)"
}

variable "ami" {
  description = "Beaver Image (AMI) to use (can be empty)"
  default     = ""
}

variable "instance_type" {
  description = "Type of Instance to start"
  default     = "t2.micro"
}

variable "subnet" {
  description = "Subnet where Instance will launch"
}

variable "instance_profile" {
  description = "IAM instance profile to launch the Instance with (can be empty)"
  default     = ""
}

variable "sshd_port" {
  description = "Just say NO to port 22!"
  default     = "22"
}

variable "timezone" {
  description = "Droplet Timezone to set. Get available timezones with: timedatectl list-timezones"
  default     = "Etc/UTC"
}

variable "reboot" {
  description = "Reboot Instance when done (set to false if you need to run your own scripts with remote-exec)"
  default     = "true"
}
